package main;

import main.concurrence.Consommateur;
import main.concurrence.Producteur;
import main.modele.Document;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class POC {
    public static void main(String[] args) {
        BlockingQueue<Document> queue = new LinkedBlockingQueue<>();
        new Thread(new Producteur(queue)).start();
        new Thread(new Consommateur(queue)).start();
    }
}
