package main.parsing;

import main.modele.Document;
import main.modele.Metadonnee;
import main.modele.ObjetNumerique;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class ParseHandler extends DefaultHandler {
    private static final String ELEMENT_OBJ_LIST_DOC = "som:documents";
    private static final String ELEMENT_OBJ_DOC = "somres:document";
    private static final String ELEMENT_OBJ_NUM = "somres:objetNumerique";
    private static final String ELEMENT_CHEMIN_NOM_FICHIER = "somres:cheminEtNomDuFichier";
    private static final String ELEMENT_LIST_METADONNEE = "somres:metadonnees";
    private static final String ELEMENT_METADONNEE = "somres:metadonnee";
    private static final String ELEMENT_CODE = "somres:code";
    private static final String ELEMENT_VALEUR = "somres:valeur";
    private static final String ELEMENT_NUM_PAGE = "somres:numeroPageDebut";
    private static final String ELEMENT_NOMBRE_PAGES = "somres:nombreDePages";

    private final BlockingQueue<Document> queue;

    private List<Document> documents = null;
    private Document doc = null;
    private ObjetNumerique objetNumerique = null;
    private StringBuilder data = null;
    private List<Metadonnee> metadonnees = null;
    private Metadonnee metadonnee = null;
    private String tmpValue;

    ParseHandler(BlockingQueue<Document> queue) {
        this.queue = queue;
    }

    @Override
    public void startElement(String uri, String localName, String elementName, Attributes attributes) {
        switch (elementName) {
            case ELEMENT_OBJ_DOC:
                doc = new Document();
                if (documents == null) documents = new ArrayList<>();
                break;
            case ELEMENT_OBJ_NUM:
                objetNumerique = new ObjetNumerique();
                break;
            case ELEMENT_LIST_METADONNEE:
                metadonnees = new ArrayList<>();
                break;
            case ELEMENT_METADONNEE:
                metadonnee = new Metadonnee();
                break;
            default:
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String element) {
        switch (element) {
            case ELEMENT_OBJ_LIST_DOC:
                Document stop = new Document();
                ObjetNumerique poison = new ObjetNumerique();
                poison.setCheminEtNomDuFichier("POISON");
                stop.setObjetNumerique(poison);
                queue.add(stop);
                System.out.println("[ Producteur ] : "+ stop.getObjetNumerique().getCheminEtNomDuFichier());
            case ELEMENT_OBJ_DOC:
                queue.add(doc);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("[ Producteur ] : "+ doc.getObjetNumerique().getCheminEtNomDuFichier());
            break;
            case ELEMENT_OBJ_NUM:
                doc.setObjetNumerique(objetNumerique);
                break;
            case ELEMENT_CHEMIN_NOM_FICHIER:
                objetNumerique.setCheminEtNomDuFichier(tmpValue);
                break;
            case ELEMENT_LIST_METADONNEE:
                doc.setMetadonnees(metadonnees);
                break;
            case ELEMENT_METADONNEE:
                metadonnees.add(metadonnee);
                break;
            case ELEMENT_CODE:
                metadonnee.setCode(tmpValue);
                break;
            case ELEMENT_VALEUR:
                metadonnee.setValeur(tmpValue);
                break;
            case ELEMENT_NUM_PAGE:
                doc.setNumeroPageDebut(Integer.parseInt(tmpValue));
                break;
            case ELEMENT_NOMBRE_PAGES:
                doc.setNombreDePages(Integer.parseInt(tmpValue));
                break;
            default:
                break;
        }
    }

    @Override
    public void characters(char ch[], int start, int length) {
        tmpValue = new String(ch, start, length);
    }
}
