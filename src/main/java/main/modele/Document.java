package main.modele;

import lombok.Data;

import java.util.List;

@Data
public class Document {
    private ObjetNumerique objetNumerique;
    private List<Metadonnee> metadonnees;
    private int numeroPageDebut;
    private int nombreDePages;
}
