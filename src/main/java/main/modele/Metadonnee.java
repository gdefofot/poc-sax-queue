package main.modele;

import lombok.Data;

@Data
public class Metadonnee {
    private String code;
    private String valeur;
}
