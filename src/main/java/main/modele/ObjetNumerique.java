package main.modele;

import lombok.Data;

@Data
public class ObjetNumerique {
    String cheminEtNomDuFichier;
}
