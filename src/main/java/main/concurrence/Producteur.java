package main.concurrence;

import lombok.AllArgsConstructor;
import main.parsing.XmlFileParser;
import main.modele.Document;

import java.util.concurrent.BlockingQueue;

@AllArgsConstructor
public class Producteur implements Runnable{
    private static final String FILE_TO_PARSE = "C:\\Users\\dje_d\\Downloads\\files\\files\\sommaire.xml";
    private final BlockingQueue<Document> queue;

    @Override
    public void run() {
        try{
            process();
        }catch (InterruptedException e){
            Thread.currentThread().interrupt();
        }

    }

    private void process() throws InterruptedException{
        XmlFileParser xmp = new XmlFileParser(queue);
        xmp.parseDocument(FILE_TO_PARSE);
    }
}
