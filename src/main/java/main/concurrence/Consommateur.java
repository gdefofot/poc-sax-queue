package main.concurrence;

import lombok.AllArgsConstructor;
import main.modele.Document;

import java.util.concurrent.BlockingQueue;

@AllArgsConstructor
public class Consommateur implements Runnable{

    private static final String POISON = "POISON";
    private final BlockingQueue<Document> queue;

    @Override
    public void run() {
        try {
            while (true){
                Thread.sleep(2000);
                Document take = queue.take();
                process(take);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void process(Document take) {
        System.out.println("[ Consommateur ] : "+ take.getObjetNumerique().getCheminEtNomDuFichier());
        if(take.getObjetNumerique().getCheminEtNomDuFichier().equalsIgnoreCase(POISON))
            Thread.currentThread().interrupt();
    }
}
